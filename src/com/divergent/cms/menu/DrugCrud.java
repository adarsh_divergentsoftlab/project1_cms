package com.divergent.cms.menu;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.divergent.cms.dao.DrugCrudDao;
import com.divergent.cms.util.ConnectionUtil;

public class DrugCrud  {
	// TODO Auto-generated constructor stub
	
	public static void crudDrugs() throws Exception {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		System.out.println("**********Hello user! Welcome to the drug Page **********");

		System.out.println("Enter 1 for create drugs");
		System.out.println("Enter 2 for update drugs");
		System.out.println("Enter 3 for delete drugs");
		System.out.println("Enter 4 for view drugs");
		System.out.println("Enter 5 for Exit");

		int choice = scan.nextInt();

		switch (choice) {

		case (1):
			createDrugs();
			break;
		case (2):
			updateDrugs();
			break;
		case (3):
			deleteDrugs();
			break;
		case (4):
			viewDrugs();
			break;
		case(5):
			Login.main(null);
		break;

		default:
			System.out.println("You have entered wrong choice");
			break;
		}

	}

	public static void viewDrugs() throws Exception {
		// TODO Auto-generated method stub

		String query = "SELECT `drugs`.`DrugID`,\r\n" + "    `drugs`.`DrugName`,\r\n"
				+ "    `drugs`.`DrugQuantity`,\r\n" + "    `drugs`.`DrugMfgDate`,\r\n"
				+ "    `drugs`.`DrugExpiryDate`\r\n" + "FROM `clinicmanagementsystem`.`drugs`;\r\n" + ";";

		// String query = "select * from doctor;";

			DrugCrudDao.viewDrug(query);
			
	}

	

	public static void deleteDrugs() throws Exception {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		System.out.println("which row do you want to delete");
		int drugId = scan.nextInt();

		DrugCrudDao.deleteDrug(drugId);

	}

	

	public static void updateDrugs() throws Exception {
		// TODO Auto-generated method stub

		while (true) {
			Scanner scan = new Scanner(System.in);
			System.out.println("Enter the drug Id to be updated");
			int DrugId = scan.nextInt();

			System.out.println("Enter your Choice");

			System.out.println("Press 1 to update drug Name");
			System.out.println("Press 2 to update drug Quantity");
			System.out.println("Press 3 to update drug Mfg date");
			System.out.println("Press 4 to update drug Expiry date");
			System.out.println("Press 5 to update Patient Name");
			System.out.println("Press 6 to update doctor Name");
			System.out.println("Press 7 to exit from drug page");
			int choice = scan.nextInt();

			switch (choice) {

			case (1): {
				updateDrugName(DrugId);
				break;
			}
			case (2): {
				updateDrugQuantity(DrugId);
				break;
			}
			case (3): {
				updateDrugMfgDate(DrugId);
				break;
			}
			case (4): {
				upadateDrugExpiryDate(DrugId);
				break;
			}
			case (5): {
				updatePatientName1(DrugId);
				break;
			}
			case (6): {
				updateDoctorName1(DrugId);
				break;
			}
			default: {
				System.out.println("You have entered wrong choice");
			}
			}
		}

	}

	public static void updateDrugName(int DrugId) throws Exception {
		// TODO Auto-generated method stub
		String query = "UPDATE `clinicmanagementsystem`.`drugs` SET `DrugName` = ? WHERE (`DrugID` = ?);";

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the Drug name:");
		String DoctorName = scan.nextLine();

	int i=	DrugCrudDao.updateDrugName(DrugId, query, DoctorName);
	if(i==0) {
		System.out.println(" Drug Name not updated");
		return;
	}
	System.out.println("Record of drug name haa been updated");

	System.out.println("Updated record of Drug name is:" + DoctorName);

	System.out.println("*********Update Successfully***********");
	}

	
	public static void updatePatientName1(int DrugId) throws Exception {
		// TODO Auto-generated method stub
		String query = "UPDATE `clinicmanagementsystem`.`drugs` SET `PatientName` = ? WHERE (`DrugID` = ?);";

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the Patient name:");
		String PatientName = scan.nextLine();

		int i = DrugCrudDao.updatePatientName1(DrugId, query, PatientName);
		if(i==0) {
			System.out.println("Patient name not updated");
			return;
		}
		System.out.println("Record of patient name has been updated");

		System.out.println("Updated record of patient name is:" + DrugId);

		System.out.println("*********Update Successfully***********");
		
	}

	
	public static void upadateDrugExpiryDate(int drugId) throws Exception {
		// TODO Auto-generated method stub
		String query = "UPDATE `clinicmanagementsystem`.`drugs` SET `DrugExpiryDate` = ? WHERE (`DrugID` = ?);";

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the Drug expiry date:");
		String DrugExpiryDate = scan.nextLine();

		int i = DrugCrudDao.updateDrugExpiryDate(drugId, query, DrugExpiryDate);
		
		if(i==0) {
			System.out.println("Expiry date not updated");
			return;}
		
		System.out.println("Record of drug expiry date has been updated");

		System.out.println("Updated record of Drug Expiry date is:" + DrugExpiryDate);

		System.out.println("*********Update Successfully***********");
		

	}

	
	public static void updateDrugMfgDate(int drugId) throws Exception {
		// TODO Auto-generated method stub
		String query = "UPDATE `clinicmanagementsystem`.`drugs` SET `DrugMfgDate` = ? WHERE (`DrugID` = ?);";

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the Drug Mfg Date:");
		String DrugMfgDate = scan.nextLine();

		int i = DrugCrudDao.updateDrugMfgDate(drugId, query, DrugMfgDate);
		
		if(i==0) {
			System.out.println("Manufacturing date not updated");
			return;}
		System.out.println("Record of drug Mfg Date has been updated");

		System.out.println("Updated record of Drug Mfg Date is:" + DrugMfgDate);

		System.out.println("*********Update Successfully***********");
	}

	

	public static void updateDrugQuantity(int drugId) throws Exception {
		// TODO Auto-generated method stub
		String query = "UPDATE `clinicmanagementsystem`.`drugs` SET `DrugQuantity` = ? WHERE (`DrugID` = ?);";

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the Drug Quantity:");
		String DrugQuantity = scan.nextLine();

		int i = DrugCrudDao.updatedrugQuantity(drugId, query, DrugQuantity);

		if(i==0) {
			System.out.println("Drug Quantity not updated");
			return;
		}
		System.out.println("Record of drug quantity has been updated");

		System.out.println("Updated record of Drug quantity is:" + DrugQuantity);

		System.out.println("*********Update Successfully***********");
		
	}

	

	public static void updateDoctorName1(int drugId) throws Exception {
		// TODO Auto-generated method stub
		String query = "UPDATE `clinicmanagementsystem`.`drugs` SET `DoctorName` = ? WHERE (`DrugID` = ?);";

		Scanner scan = new Scanner(System.in);
		System.out.println("Enter the Doctor name:");
		String DoctorName = scan.nextLine();

		int i = DrugCrudDao.updateDoctorName1(drugId, query, DoctorName);
		if(i==0) {
			System.out.println("Doctor name not updated");
			return;
		}

		System.out.println("Record of doctor name haa been updated");

		System.out.println("Updated record of Doctor name is:" + DoctorName);

		System.out.println("*********Update Successfully***********");
		
	}

	

	public static void createDrugs() throws Exception {
		// TODO Auto-generated method stub
		
		String query = "INSERT INTO `clinicmanagementsystem`.`drugs` (`DrugName`, `DrugQuantity`, `DrugMfgDate`, `DrugExpiryDate`, `PatientName`, `DoctorName`) VALUES (?, ?, ?, ?, ?, ?);\r\n"
				+ ";";

		Scanner scanner = new Scanner(System.in);

		// Taking input of Drug Name
		System.out.println("Enter the Drug Name: ");
		String DrugrName = scanner.nextLine();

		// Taking input of Drug Quantity
		System.out.println("Enter the Drug Quantity: ");
		String DrugQuantity = scanner.nextLine();

		// Taking input of MfgDate
		System.out.println("Enter the Mfg Date: ");
		String MfgDate = scanner.nextLine();

		// Taking input of ExpiryDate
		System.out.println("Enter the ExpiryDate: ");
		String ExpiryDate = scanner.nextLine();

		// Taking input of Patient Name
		System.out.println("Enter the Patient Name: ");
		String PatientName = scanner.nextLine();

		// Taking input of Doctor Name
		System.out.println("Enter the Doctor Name: ");
		String DoctorName = scanner.nextLine();

		int i= DrugCrudDao.createDrug(query, DrugrName, DrugQuantity, MfgDate, ExpiryDate, PatientName, DoctorName);

		if(i==0) {
			System.out.println("Drug not created");
			return;
		}
		System.out.println("The record of Drug has been Created: ");
		System.out.println("Name of the Drug is: " + DrugrName);
		System.out.println("Quantity of Drug is: " + DrugQuantity);
		System.out.println("Manufacturing date of drug is: " + MfgDate);
		System.out.println("Expiry date of drug is: " + ExpiryDate);
		System.out.println("NAme of Patient is : " + ExpiryDate);
		System.out.println("Name of Doctor is: " + ExpiryDate);
		System.out.println("***************************Drug Created Successfully********************************");
		



	}

}



