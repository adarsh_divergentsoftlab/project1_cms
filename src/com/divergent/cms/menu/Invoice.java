package com.divergent.cms.menu;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.divergent.cms.dao.InvoiceDao;
import com.divergent.cms.util.ConnectionUtil;

public class Invoice {

	public static void invoice() throws Exception {

		while (true) {
			Scanner scan = new Scanner(System.in);
			System.out.println("*********Hello user! Welcome to Invoice Page *********");

			System.out.println("Enter 1 for create invoice");
			System.out.println("Enter 2 for view invoice");
			System.out.println("Enter 3 for Exit");

			int choice = scan.nextInt();

			switch (choice) {
			case (1): {
				createInvoice();
				break;
			}
			case (2): {
				viewInvoice();
				break;
			}
			case (3): {
				Login.main(null);
				break;
			}

			}
		}
	}

	public static void viewInvoice() throws Exception {

		String query = "SELECT `invoicemanagement`.`patientName`,\r\n" + "    `invoicemanagement`.`TestName`,\r\n"
				+ "    `invoicemanagement`.`DoctorFees`,\r\n" + "    `invoicemanagement`.`TestFees`,\r\n"
				+ "    `invoicemanagement`.`InvoiceID`\r\n" + "FROM `clinicmanagementsystem`.`invoicemanagement`;\r\n"
				+ ";";
		// String query = "select * from doctor;";

		System.out.println("Details of Invoice are given below:");

		InvoiceDao.viewInvoice(query);
	}

	public static void createInvoice() throws Exception {

		String query = "INSERT INTO `clinicmanagementsystem`.`invoicemanagement` (`patientName`, `TestName`, `DoctorFees`, `TestFees`) VALUES (?, ?, ?, ?);\r\n"
				+ ";";

		Scanner scanner = new Scanner(System.in);

		// Taking input of Patient Name
		System.out.println("Enter the Patient Name: ");
		String PatientName = scanner.nextLine();

		// Taking input of Test Name
		System.out.println("Enter the Test Name: ");
		String TestName = scanner.nextLine();

		// Taking input of Doctor Fees
		System.out.println("Enter the Doctor Fees: ");
		String DoctorFees = scanner.nextLine();

		// Taking input of TestFees
		System.out.println("Enter the TestFees: ");
		String TestFees = scanner.nextLine();

		int i = InvoiceDao.createInvoice(query, PatientName, TestName, DoctorFees, TestFees);
		
		if(i==0) {
			System.out.println("Invoice not Created");
			return;
		}
		System.out.println("The record of Drug has been Created: ");
		System.out.println("Name of the Patient is: " + PatientName);
		System.out.println("Name of Test is: " + TestName);
		System.out.println("Charge of Doctor is: " + DoctorFees);
		System.out.println("Charge of Test is: " + TestFees);

		System.out
				.println("***************************Invoice Created Successfully********************************");
		

	}

}
