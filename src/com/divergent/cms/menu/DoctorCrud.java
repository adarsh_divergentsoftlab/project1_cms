package com.divergent.cms.menu;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.divergent.cms.dao.AppointmentDao;
import com.divergent.cms.dao.DoctorCrudDao;
import com.divergent.cms.dto.Doctor;
import com.divergent.cms.util.ConnectionUtil;

public class DoctorCrud {

	public static void crudDoctor() throws Exception {
		// TODO Auto-generated method stub
		while (true) {
			Scanner scan = new Scanner(System.in);
			System.out.println("**********Hello user! Welcome to the CRUD Doctor Page**********");

			System.out.println("Enter 1 for create Doctor");
			System.out.println("Enter 2 for update Doctor");
			System.out.println("Enter 3 for delete Doctor");
			System.out.println("Enter 4 for view Doctor");
			System.out.println("Enter 5 for exit");

			int choice = scan.nextInt();

			switch (choice) {

			case (1):
				createDoctor();
				break;
			case (2):
				updateDoctor();
				break;
			case (3):
				deleteDoctor();
				break;
			case (4):
				viewDoctor();
				break;
			case (5):
				Login.main(null);
				break;

			default:
				System.out.println("You have entered wrong choice");
				break;
			}
		}
	}

	public static void createDoctor() throws Exception {
		// TODO Auto-generated method stub

		String query = "INSERT INTO `clinicmanagementsystem`.`doctor` (`DoctorName`, `DoctorSpecialization`, `DoctorFees`, `DoctorDegree`)"
				+ " VALUES (?, ?, ?, ?);\r\n" + "; ";

		Scanner scanner = new Scanner(System.in);

		// Taking input of Doctor Name
		System.out.println("Enter the Doctor Name: ");
		String DoctorName = scanner.nextLine();

		// Taking input of Doctor Specialization
		System.out.println("Enter the Doctor Specialization: ");
		String DoctorSpecialization = scanner.nextLine();

		// Taking input of Doctor Fees
		System.out.println("Enter the Doctor Fees: ");
		String DoctorFees = scanner.nextLine();

		// Taking input of Doctor Degree
		System.out.println("Enter the Doctor Degree: ");
		String DoctorDegree = scanner.nextLine();

		int i = DoctorCrudDao.createDoctor(query, DoctorName, DoctorSpecialization, DoctorFees, DoctorDegree);

		if (i == 0) {
			System.out.println("doc not created ");
			return;
		}

		System.out.println("The record of Doctor has been Created: ");
		System.out.println("Name of the Doctor is: " + DoctorName);
		System.out.println("Specilization of the Doctor is: " + DoctorSpecialization);
		System.out.println("Fees of the Doctor is: " + DoctorFees);
		System.out.println("Degree of the Doctor is: " + DoctorDegree);
		System.out.println("***************************Doctor Created Successfully********************************");
	}

	public static void viewDoctor() throws Exception {
		// TODO Auto-generated method stub

		String query = "SELECT `doctor`.`doctorID`,\r\n" + "    `doctor`.`DoctorName`,\r\n"
				+ "    `doctor`.`DoctorSpecialization`,\r\n" + "    `doctor`.`DoctorFees`,\r\n"
				+ "    `doctor`.`DoctorDegree`\r\n" + "FROM `clinicmanagementsystem`.`doctor`;\r\n" + " ";

//		Scanner scan = new Scanner(System.in);
//		System.out.println("Enter the doctor Id : ");
//		int doctorId = scan.nextInt();
		// String query = "select * from doctor where doctorID = " + doctorId + ";";

		DoctorCrudDao.viewDoctor(query);

	}

	public static void deleteDoctor() throws Exception {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		System.out.println("Which row do you want to delete");
		int doctorId = scan.nextInt();

		String query = "delete from doctor where doctorID = ?";
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {

			st.setString(1, "" + doctorId);

			int rowsDeleted = st.executeUpdate();
			if (rowsDeleted > 0) {
				System.out.println("********* deleted successfully!*********");
				System.exit(0);
			}
		}

	}

	public static void updateDoctor() throws Exception {
		// TODO Auto-generated method stub

		while (true) {

			Scanner scan = new Scanner(System.in);

			System.out.println("Enter the Doctor ID to be updated");
			int doctorId = scan.nextInt();

			System.out.println("Enter your choice");

			System.out.println("Enter 1 for update doctor name");
			System.out.println("Enter 2 for update doctor specialization");
			System.out.println("Enter 3 for update fees of Doctor");
			System.out.println("Enter 4 for Update degree of Doctor");
			System.out.println("Enter 5 for exit Page");
			int choice = scan.nextInt();

			switch (choice) {
			case (1):
				updateDoctorName1(doctorId);
				break;
			case (2):
				updateDoctorSpecialization(doctorId);
				break;
			case (3):
				updateDoctorFees(doctorId);
				break;
			case (4):
				updateDoctorDegree(doctorId);
				break;

			default:
				System.out.println("you hav eentered wrong choice:");
				break;

			}
		}
	}

	public static void updateDoctorName1(int doctorId) throws Exception {
		// TODO Auto-generated method stub
		String query = "UPDATE `clinicmanagementsystem`.`doctor` SET  `DoctorName` = ? WHERE (`doctorID` = ?);";

		Scanner scanner = new Scanner(System.in);

		System.out.println(" update the doctor Name");
		String updateDoctorName = scanner.nextLine();

		int i = DoctorCrudDao.updateDoctorName1(doctorId, query, updateDoctorName);

		if (i == 0) {
			System.out.println("doc not created ");
			return;
		}

		System.out.println("The record of Doctor has been Updated: ");

		System.out.println("Updated Name of the Doctor is: " + updateDoctorName);
		System.out.println(
				"*************************** updated record of Doctor Name Created Successfully********************************");

	}

	public static void updateDoctorSpecialization(int doctorId) throws Exception {
		// TODO Auto-generated method stub
		String query = "UPDATE `clinicmanagementsystem`.`doctor` SET  `DoctorSpecilization` = ? WHERE (`doctorID` = ?);";

		Scanner scanner = new Scanner(System.in);

		System.out.println(" update the doctor specilization");
		String updateDoctorSpecilization = scanner.nextLine();

		int i = DoctorCrudDao.updateDoctorSpecilization(doctorId, query, updateDoctorSpecilization);

		if (i == 0) {
			System.out.println("Doc not Updated");
			return;
		}
	}

	public static void updateDoctorFees(int doctorId) throws Exception {
		// TODO Auto-generated method stub
		String query = "UPDATE `clinicmanagementsystem`.`doctor` SET  `DoctorFees` = ? WHERE (`doctorID` = ?);";

		Scanner scanner = new Scanner(System.in);

		System.out.println(" update the doctor Fees");
		String updateDoctorFees = scanner.nextLine();

		int i = DoctorCrudDao.updateDoctorFees(doctorId, query, updateDoctorFees);
		if (i == 0) {
			System.out.println("Doc not updated");
			return;
		}

	}

	public static void updateDoctorDegree(int doctorId) throws Exception {
		// TODO Auto-generated method stub

		String query = "UPDATE `clinicmanagementsystem`.`doctor` SET  `DoctorDegree` = ? WHERE (`doctorID` = ?);";

		Scanner scanner = new Scanner(System.in);

		System.out.println(" update the doctor degree");
		String updateDoctorDegree = scanner.nextLine();

		int i = DoctorCrudDao.updateDoctorDegree(doctorId, query, updateDoctorDegree);
		if (i == 0) {
			System.out.println("Doc not updated");
			return;
		}
			System.out.println("The record of Doctor has been Updated: ");

			System.out.println("Updated Degree of the Doctor is: " + updateDoctorDegree);
			System.out.println(
					"*************************** updated record of Doctor Degree Created Successfully********************************");
		}

	}

