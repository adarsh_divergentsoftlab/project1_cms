package com.divergent.cms.menu;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.divergent.cms.dao.LabTestDao;
import com.divergent.cms.util.ConnectionUtil;

public class LabTestCrud {

	public static void crudLabtest() throws Exception {
		// TODO Auto-generated method stub

		while (true) {
			Scanner scan = new Scanner(System.in);
			System.out.println("**********Hello user! Welcome to the CRUD LabTest Page**********");

			System.out.println("Enter 1 for create lab test");
			System.out.println("Enter 2 for update lab  test");
			System.out.println("Enter 3 for delete lab test");
			System.out.println("Enter 4 for view lab test");
			System.out.println("Enter 5 for Exit");
			int choice = scan.nextInt();

			switch (choice) {

			case (1):
				createTest();
				break;
			case (2):
				updateTest();
				break;
			case (3):
				deleteTest();
				break;
			case (4):
				viewTest();
				break;
			case (5):
				System.exit(0);
				break;

			default:
				System.out.println("You have entered wrong choice");
				break;
			}
		}
	}

	public static void viewTest() throws Exception {
		// TODO Auto-generated method stub

		String query = "SELECT `labtest`.`ReportId`,\r\n" + "    `labtest`.`TestName`,\r\n"
				+ "    `labtest`.`TestFees`\r\n" + "FROM `clinicmanagementsystem`.`labtest`;\r\n" + ";";
		// String query = "select * from LabTest;";

		LabTestDao.viewTest(query);

	}

	
	public static void deleteTest() throws Exception {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		System.out.println("Which row  do you want to delete");
		int ReportID = scan.nextInt();

		LabTestDao.deleteLabTest(ReportID);
	}

	

	public static void updateTest() throws Exception {
		// TODO Auto-generated method stub

		while (true) {

			Scanner scan = new Scanner(System.in);

			System.out.println("Enter the Report ID to be updated");
			int reportId = scan.nextInt();

			System.out.println("Enter your choice");

			System.out.println("Enter 1 for update doctor name");
			System.out.println("Enter 2 for update Patient Name");
			System.out.println("Enter 3 for update fees of Test");
			System.out.println("Enter 4 for Update Name of Test");
			System.out.println("Enter 5 for exit Page");
			int choice = scan.nextInt();

			switch (choice) {
			case (1):
				updateDoctorName(reportId);
				break;
			case (2):
				updatePatientName(reportId);
				break;
			case (3):
				updateTestFees(reportId);
				break;
			case (4):
				updateTestName(reportId);
				break;

			default:
				System.out.println("you have entered wrong choice:");
				break;
			}
		}
	}

	public static void updateDoctorName(int reportId) throws Exception {
		// TODO Auto-generated method stub
		String query = " UPDATE `clinicmanagementsystem`.`labtest` SET `DoctorName` = ? WHERE (`ReportId` = ?);";

		Scanner scanner = new Scanner(System.in);

		System.out.println(" update the doctor Name");
		String updateDoctorName = scanner.nextLine();

		int i=LabTestDao.updateDoctorName(reportId, query, updateDoctorName);
		
		if(i==0) {
			System.out.println("Doctor name not updated");
			return;
		}
		System.out.println("The record of Doctor has been Updated: ");

		System.out.println("Updated Name of the Doctor is: " + updateDoctorName);
		System.out.println(
				"*************************** updated record of Doctor Name Created Successfully********************************");

	}

	
	public static void updatePatientName(int reportId) throws Exception {
		// TODO Auto-generated method stub
		String query = " UPDATE `clinicmanagementsystem`.`labtest` SET `PatientName` = ? WHERE (`ReportId` = ?);";

		Scanner scanner = new Scanner(System.in);

		System.out.println(" update the Patient Name");
		String updatePatientName = scanner.nextLine();

		int i=LabTestDao.updatePatientName(reportId, query, updatePatientName);
		if(i==0) {
			System.out.println("Patient name not updated");
			return;
		}
		System.out.println("The record of Patient has been Updated: ");

		System.out.println("Updated Name of Patient is: " + updatePatientName);
		System.out.println(
				"*************************** updated record of Doctor Specialization Created Successfully********************************");
	}

	
	public static void updateTestFees(int reportId) throws Exception {
		// TODO Auto-generated method stub
		String query = " UPDATE `clinicmanagementsystem`.`labtest` SET `TestFees` = ? WHERE (`ReportId` = ?);";

		Scanner scanner = new Scanner(System.in);

		System.out.println(" update the Test Fees");
		String updateTestFees = scanner.nextLine();

		int i=LabTestDao.updateTestFees(reportId, query, updateTestFees);
		if(i==0) {
			System.out.println("Test Fees not updated");
		}
		System.out.println("The record of Test Fees has been Updated: ");

		System.out.println("Updated Fees of the Test is: " + updateTestFees);
		System.out.println(
				"*************************** updated record of Doctor Fees Created Successfully********************************");

	}

	

	public static void updateTestName(int reportId) throws Exception {
		// TODO Auto-generated method stub

		String query = " UPDATE `clinicmanagementsystem`.`labtest` SET `TestName` = ? WHERE (`ReportId` = ?);";
		Scanner scanner = new Scanner(System.in);

		System.out.println(" update the Test Name");
		String updateTestName = scanner.nextLine();

		int i=LabTestDao.updateTestName(reportId, query, updateTestName);
		if(i==0) {
			System.out.println("Test name not updated");
		}
		if(i==0) {
			System.out.println("Test Fees not updated");
		}
	}

	

	public static void createTest() throws Exception {
		// TODO Auto-generated method stub

		String query = "INSERT INTO `clinicmanagementsystem`.`labtest` ( `TestName`, `TestFees`, `PatientName`, `DoctorName`) VALUES ( ?,?,?,?);\r\n"
				+ ";";

		Scanner scanner = new Scanner(System.in);

		// Taking input of Test Name
		System.out.println("Enter the Test Name: ");
		String TestName = scanner.nextLine();

		// Taking input of Test Fees
		System.out.println("Enter the Test Fees: ");
		String TestFees = scanner.nextLine();

		// Taking input of Patient Name
		System.out.println("Enter the Patient Name: ");
		String PatientName = scanner.nextLine();

		// Taking input of DoctorName
		System.out.println("Enter the Doctor Name: ");
		String DoctorName = scanner.nextLine();

		int i=LabTestDao.createTestName(query, TestName, TestFees, PatientName, DoctorName);
		
		if(i==0) {
			System.out.println("Test  not created");
		}
		
		
		System.out.println("The record of LabTest has been Created: ");
		System.out.println("Name of the LabTest is: " + TestName);
		System.out.println("Charge of Test is: " + TestFees);
		System.out.println("Name of Patient is: " + TestFees);
		System.out.println("Name of Doctor is : " + TestFees);

		System.out
				.println("***************************LabTest Created Successfully********************************");


	}

	
}
