package com.divergent.cms.menu;

import java.util.Scanner;

public class Login {
	public static void main(String[] Args) throws Exception {

		while (true) {
			Scanner scan = new Scanner(System.in);
			System.out.println("\t***********WELCOME TO CLINIC MANAGEMENT SYSTEM**********\n");

			System.out.println("Please let me know! Which mode do you want?\n");

			System.out.println("Enter 1 for Admin mode");
			System.out.println("Enter 2 for Patient mode");
			System.out.println("Enter 3 for Doctor mode");
			System.out.println("Enter 4 for exit");

			int choice = scan.nextInt();

			switch (choice) {
			case (1): {

				System.out.println("**********Hello user! Welcome to Admin Page**********");

				System.out.println("Enter 1 for CRUD Doctor");
				System.out.println("Enter 2 for CRUD Patient ");
				System.out.println("Enter 3 for format Invoice");
				System.out.println("Enter 4 for format Appointment");
				System.out.println("Enter 5 for CRUD Drug");
				System.out.println("Enter 6 for CRUD LabTest");
				System.out.println("Enter 7 for previous page");

				int choice2 = scan.nextInt();

				switch (choice2) {
				case (1): {
					DoctorCrud.crudDoctor();
					break;
				}
				case (2): {
					PatientCrud.crudPatient();
				}
				case (3): {
					Invoice.invoice();
				}
				case (4): {
					Appointment.appointment();
				}
				case (5): {
					DrugCrud.crudDrugs();
				}
				case (6): {
					LabTestCrud.crudLabtest();
				}
				case (7): {
					Login.main(null);
				}
				}
			}
			case (2): {

				System.out.println("**********Hello user! Welcome to Patient Page**********");

				System.out.println("Enter 1 format patient detail");
				System.out.println("Enter 2 for appointmnet");
				System.out.println("Enter 3 for invoice");
				System.out.println("Enter 4 for previous page");

				int choice1 = scan.nextInt();

				switch (choice1) {
				case (1): {
					PatientCrud.crudPatient();
				}
				case (2): {
					Appointment.appointment();
				}
				case (3): {
					Invoice.invoice();
				}
				case (4): {
					Login.main(null);
				}
				}

				break;
			}
			case (3): {

				System.out.println("**********Hello Doctor! Welcome to the Page**********");

				System.out.println("Enter 1 format CRUD Doctor");
				System.out.println("Enter 2 for CRUD drugs");
				System.out.println("Enter 3 for Crud labtest");
				System.out.println("Enter 4 for previous page");

				int choice2 = scan.nextInt();

				switch (choice2) {
				case (1): {
					DoctorCrud.crudDoctor();
					break;
				}
				case (2): {
					DrugCrud.crudDrugs();
					break;
				}
				case (3): {
					LabTestCrud.crudLabtest();
					break;
				}
				case (4): {
					Login.main(null);
				}
				}
			}
			case (4): {
				System.out.println("**********Your program has been terminated**********");
				System.exit(0);
			}

			default: {

				System.out.println("You have entered wrong choice");
				break;

			}
			}

		}
	}
}
