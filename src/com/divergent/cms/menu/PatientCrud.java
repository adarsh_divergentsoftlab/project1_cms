package com.divergent.cms.menu;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.divergent.cms.dao.PatientCrudDao;
import com.divergent.cms.util.ConnectionUtil;

public class PatientCrud {

	public static void crudPatient() throws Exception {

		while (true) {
			// TODO Auto-generated method stub

			Scanner scan = new Scanner(System.in);
			System.out.println("**********Hello user! Welcome to the CRUD Patient Page**********");

			System.out.println("Enter 1 for create Patient");
			System.out.println("Enter 2 for update  Patient");
			System.out.println("Enter 3 for delete  Patient");
			System.out.println("Enter 4 for view  Patient");
			System.out.println("Enter 5 for Exit");

			int choice = scan.nextInt();

			switch (choice) {

			case (1):
				createPatient();
				break;
			case (2):
				updatePatient();
				break;
			case (3):
				deletePatient();
				break;
			case (4):
				viewPatient();
				break;
			case (5):
				Login.main(null);
				break;

			default:
				System.out.println("You have entered wrong choice");
				break;
			}
		}
	}

	public static void viewPatient() throws Exception {
		// TODO Auto-generated method stub

//		String query = "SELECT `patient`.`PatientID`,\r\n" + "    `patient`.`FName`,\r\n" + "    `patient`.`LName`,\r\n"
//				+ "    `patient`.`PatientAge`,\r\n" + "    `patient`.`PatientNumber`,\r\n"
//				+ "    `patient`.`PatientAddress`\r\n" + "FROM `clinicmanagementsystem`.`patient`;\r\n" + "";

		 String query = "select * from patient;";

		PatientCrudDao.viewPatient(query);

	}

	public static void deletePatient() throws Exception {
		// TODO Auto-generated method stub

		Scanner scan = new Scanner(System.in);
		System.out.println("which row do you want to delete");
		int patientId = scan.nextInt();

		PatientCrudDao.deletePatient(patientId);

	}

	public static void updatePatient() throws Exception {
		// TODO Auto-generated method stub

		while (true) {

			Scanner scan = new Scanner(System.in);

			System.out.println("Enter the Patient ID to be updated");
			int patientId = scan.nextInt();

			System.out.println("Enter your choice");

			System.out.println("Enter 1 for update first name");
			System.out.println("Enter 2 for update last name");
			System.out.println("Enter 3 for update age of patient");
			System.out.println("Enter 4 for Update number of patient");
			System.out.println("Enter 5 for Update address of patient");
			System.out.println("Enter 6 for exit Page");
			int choice = scan.nextInt();

			switch (choice) {
			case (1):
				updateFName(patientId);
				break;
			case (2):
				updateLName(patientId);
				break;
			case (3):
				updatePatientAge(patientId);
				break;
			case (4):
				updatePatientNumber(patientId);
				break;
			case (5): {
				updatePatientAddress(patientId);
				break;
			}
			default:
				System.out.println("you hav eentered wrong choice:");
				break;
			}
		}
	}

	public static void updateFName(int patientId) throws Exception {
		// TODO Auto-generated method stub
		String query = "UPDATE `clinicmanagementsystem`.`patient` SET  `FName` = ? WHERE (`patientId` = ?);";

		Scanner scanner = new Scanner(System.in);

		System.out.println(" update the patient first Name");
		String updateFName = scanner.nextLine();

		int i= PatientCrudDao.updateFName(patientId, query, updateFName);
		if(i==0) {
			System.out.println("First name is not updated");
			return;
		}
		System.out.println("The record of patient first name has been Updated: ");

		System.out.println("Updated first Name of the patient is: " + updateFName);
		System.out.println(
				"*************************** updated record of patient first Name Created Successfully********************************");

	}

	public static void updateLName(int patientId) throws Exception {
		// TODO Auto-generated method stub
		String query = "UPDATE `clinicmanagementsystem`.`patient` SET  `LName` = ? WHERE (`patientId` = ?);";

		Scanner scanner = new Scanner(System.in);

		System.out.println(" update the patient last Name");
		String updateLName = scanner.nextLine();

		int i = PatientCrudDao.updateLName(patientId, query, updateLName);
		
		if(i==0) {
			System.out.println("Last name is not updated");
			return;}
		System.out.println("The record of patient last name has been Updated: ");

		System.out.println("Updated last Name of the patient is: " + updateLName);
		System.out.println(
				"*************************** updated record of patient last Name Created Successfully********************************");

	}

	public static void updatePatientAge(int patientId) throws Exception {
		// TODO Auto-generated method stub
		String query = "UPDATE `clinicmanagementsystem`.`patient` SET  `PatientAge` = ? WHERE (`patientId` = ?);";

		Scanner scanner = new Scanner(System.in);

		System.out.println(" update the Age of patient");
		String updatePatientAge = scanner.nextLine();

		int i=PatientCrudDao.updatePatientAge(patientId, query, updatePatientAge);
		
		if(i==0) {
			System.out.println("Age not updated");
			return;
		}
		System.out.println("The record of age of patient has been Updated: ");

		System.out.println("Updated age of patient is: " + updatePatientAge);
		System.out.println(
				"*************************** updated record of age of patient Created Successfully********************************");
	}

	public static void updatePatientNumber(int doctorId) throws Exception {
		// TODO Auto-generated method stub
		String query = "UPDATE `clinicmanagementsystem`.`patient` SET  `PatientNumber` = ? WHERE (`patientId` = ?);";

		Scanner scanner = new Scanner(System.in);

		System.out.println(" update the number of patient");
		String updatePatientNumber = scanner.nextLine();

		int i=PatientCrudDao.updatePatientNumber(doctorId, query, updatePatientNumber);
		
		if(i==0) {
			System.out.println("Number is not updated");
			return;
		}
		System.out.println("The record ofnumber of patient has been Updated: ");

		System.out.println("Updated number of patient is: " + updatePatientNumber);
		System.out.println(
				"*************************** updated record of patient number Created Successfully********************************");
		

	}

	public static void updatePatientAddress(int patientId) throws Exception {
		// TODO Auto-generated method stub

		String query = "UPDATE `clinicmanagementsystem`.`patient` SET  `PatientAddress` = ? WHERE (`patientId` = ?);";

		Scanner scanner = new Scanner(System.in);

		System.out.println(" update the address of patient");
		String updatePatientAddress = scanner.nextLine();

		int i = PatientCrudDao.updatepatientAddress(patientId, query, updatePatientAddress);
		
		if(i==0) {
			System.out.println("Last name is not updated");
			return;}

		System.out.println("The record of address of patient has been Updated: ");

		System.out.println("Updated address of patient is: " + updatePatientAddress);
		System.out.println(
				"*************************** updated record of address of patient Created Successfully********************************");
	

	}

	public static void createPatient() throws Exception {
		// TODO Auto-generated method stub

		String query = "insert into patient(FName,LName,PatientAge,PatientNumber,PatientAddress) values(?,?,?,?,?)";

		Scanner scanner = new Scanner(System.in);

		// Taking input of Patient FName
		System.out.println("Enter the Patient first Name: ");
		String FName = scanner.nextLine();

		// Taking input of Last Name
		System.out.println("Enter the Patient Last Name: ");
		String LName = scanner.nextLine();

		// Taking input of Patient Age
		System.out.println("Enter the Patient Age: ");
		String PatientAge = scanner.nextLine();

		// Taking input of Patient Number
		System.out.println("Enter the Patient Number : ");
		String PatientNumber = scanner.nextLine();

		// Taking Input of Patient Address
		System.out.println("Enter the Patient Address: ");
		String PatientAddress = scanner.nextLine();

		int i= PatientCrudDao.createPatient(query, FName, LName, PatientAge, PatientNumber, PatientAddress);
		if(i==0) {
			System.out.println("Last name is not updated");
			return;}
		System.out.println("The record of Patient has been Created: ");
		System.out.println("First Name of the Patient is: " + FName);
		System.out.println("Last Name of the Patient is: " + LName);
		System.out.println("Age of Patient is: " + PatientAge);
		System.out.println("Contact number of Patient is: " + PatientNumber);
		System.out.println("Address of Patient is: " + PatientAddress);
		System.out
				.println("***************************Patient Created Successfully********************************");
		


	}

}
