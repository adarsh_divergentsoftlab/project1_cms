package com.divergent.cms.menu;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.Scanner;

import com.divergent.cms.dao.AppointmentDao;
import com.divergent.cms.util.ConnectionUtil;

public class Appointment {

	public static void appointment() throws Exception {

		while (true) {
			Scanner scan = new Scanner(System.in);

			System.out.println("**********Hello user! Welcome to the Appointment Page**********");

			System.out.println("Enter 1 for create appointment");
			System.out.println("Enter 2 for view appointment");
			System.out.println("Enter 3 for Exit");

			int choice = scan.nextInt();

			switch (choice) {
			case (1): {
				createAppointment();
				break;
			}
			case (2): {
				viewAppointment();
				break;
			}
			case (4): {
				Login.main(null);
				break;
			}

			}
		}
	}

	public static void createAppointment() throws Exception {

		String query = "INSERT INTO `clinicmanagementsystem`.`onlineappointment` (`patientName`, `patientNumber`, `AllotedDoctor`, `AllottedDate`) VALUES (?, ?, ?,?);\r\n"
				+ "";
		/*
		 * try(Connection connect = ConnectionProvider.provideConnection();
		 * PreparedStatement pst = connect.createPreparedStatement() ){
		 * 
		 * 
		 * }
		 * 
		 * 
		 */
		Scanner scanner = new Scanner(System.in);

		// Taking input of Patient Name
		System.out.println("Enter the Patient Name: ");
		String patientName = scanner.nextLine();

		// Taking input of Patient Number
		System.out.println("Enter the Patient Number: ");
		String patientNumber = scanner.nextLine();

		// Taking input of Patient Number
		System.out.println("Enter the Allotted Doctor: ");
		String AllottedDoctor = scanner.nextLine();

		// Taking input of Allotted Date
		System.out.println("Enter the Alloted Date: ");
		String AllottedDate = scanner.nextLine();

		int i = AppointmentDao.createAppointment(query, patientName, patientNumber, AllottedDoctor, AllottedDate);
		if (i == 0) {
			System.out.println("Appointment not created");
			return;
		}
		System.out.println("The Appointment has been Booked: ");
		System.out.println("Name of the Patient is: " + patientName);
		System.out.println("Name of Test is: " + patientNumber);
		System.out.println("Charge of Doctor is: " + AllottedDoctor);
		System.out.println("Charge of Test is: " + AllottedDate);

		System.out
				.println("***************************Appointment Booked Successfully********************************");

	}

	public static void viewAppointment() throws Exception {

		String query = "SELECT `onlineappointment`.`patientName`,\r\n" + "    `onlineappointment`.`patientNumber`,\r\n"
				+ "    `onlineappointment`.`AllotedDoctor`,\r\n" + "    `onlineappointment`.`DoctorID`\r\n"
				+ "FROM `clinicmanagementsystem`.`onlineappointment`;\r\n" + ";";
		// String query = "select * from doctor;";

		AppointmentDao.viewAppointment(query);
	}

}
