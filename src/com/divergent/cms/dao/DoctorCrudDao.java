package com.divergent.cms.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.divergent.cms.dto.Doctor;
import com.divergent.cms.util.ConnectionUtil;

public class DoctorCrudDao {

	public static int createDoctor(String query, String DoctorName, String DoctorSpecilization, String DoctorFees,
			String DoctorDegree) throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {

			st.setString(1, DoctorName);
			st.setString(2, DoctorSpecilization);
			st.setString(3, DoctorFees);
			st.setString(4, DoctorDegree);

			int i = st.executeUpdate();
			
			if(i ==1)
				return 1;
			else 
				return 0;

		}
	}

	public static void viewDoctor(String query) throws SQLException, IOException {
		System.out.println("Details of Doctor are given below:");

		List<Doctor> result =  new ArrayList<>();
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {

			Doctor doctor = null;
			ResultSet rs = st.executeQuery(query);
			
			while (rs.next()) {
//				System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " "
//						+ rs.getString(4) + " " + rs.getString(5));
//		
				doctor = new Doctor();
				doctor.setDoctorName(rs.getString(1));
				doctor.setDoctorSpecialiation(rs.getString(2));
				doctor.setDoctorFees(rs.getInt(1));
				doctor.setDoctorDegree(rs.getString(1));
				result.add(doctor);
			}
			
		}
	}

	public static int updateDoctorName1(int doctorId, String query, String updateDoctorName)
			throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, updateDoctorName);
			st.setString(2, "" + doctorId);

			int i = st.executeUpdate();
			
			if(i ==1)
				return 1;
			else 
				return 0;

			
		
		}
	}

	public static int updateDoctorSpecilization(int doctorId, String query, String updateDoctorSpecilization)
			throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, updateDoctorSpecilization);
			st.setString(2, "" + doctorId);

			int i = st.executeUpdate();

			if (i == 1) 
				return 1;
			else return 0;
			

			
		}
	}

	public static int updateDoctorFees(int doctorId, String query, String updateDoctorFees)
			throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, updateDoctorFees);
			st.setString(2, "" + doctorId);

			int i = st.executeUpdate();

			if(i == 0)
				return 1;
			else
				return 0;

			
			
		}
	}

	public static int updateDoctorDegree(int doctorId, String query, String updateDoctorDegree)
			throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, updateDoctorDegree);
			st.setString(2, "" + doctorId);

			int i = st.executeUpdate();

			if(i == 0)
				return 1;
			else return 0;

			
			
		}
	}

}
