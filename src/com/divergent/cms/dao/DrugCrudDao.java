package com.divergent.cms.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.divergent.cms.dto.Drugs;
import com.divergent.cms.util.ConnectionUtil;

public class DrugCrudDao {
	
	public static void viewDrug(String query) throws SQLException, IOException {
		System.out.println("Details of Drug are given below:");

		List<Drugs> result = new ArrayList<>();
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {

			Drugs drugs = null;
			ResultSet rs = st.executeQuery(query);

			while (rs.next()) {
//				System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " "
//						+ rs.getString(4) + " " + rs.getString(5));
//				System.exit(0);
				drugs = new Drugs();
				drugs.setDrugName(rs.getString(1));
				drugs.setDrugQuantity(rs.getInt(2));
				drugs.setMgfDate(rs.getInt(3));
				drugs.setExpiryDate(rs.getInt(4));
				drugs.setPatientName(rs.getString(5));
				drugs.setDoctorName(rs.getString(6));
				result.add(drugs);			}

		}
	}
	public static void deleteDrug(int drugId) throws SQLException, IOException {
		String query = "delete from drugs where DrugID = ?";
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {

			st.setString(1, "" + drugId);

			int rowsDeleted = st.executeUpdate();
			if (rowsDeleted > 0) {
				System.out.println("Drugs was deleted successfully!");
				
			}
		}
	}
	public static int updateDrugName(int DrugId, String query, String DoctorName) throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, DoctorName);
			st.setString(2, "" + DrugId);

			int i = st.executeUpdate();
			if(i==0)
				return 1;
			else 
				return 0;
			
			

		}
	}
	public static int updatePatientName1(int DrugId, String query, String PatientName)
			throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, PatientName);
			st.setString(2, "" + DrugId);

			int i = st.executeUpdate();
			if(i==0)
				return 1;
			else 
				return 0;
			

			

		}
	}
	public static int updateDrugExpiryDate(int drugId, String query, String DrugExpiryDate)
			throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, DrugExpiryDate);
			st.setString(2, "" + drugId);

			int i = st.executeUpdate();
			if(i==0)
				return 1;
			else 
				return 0;
			

			
		}
	}
	public static int updateDrugMfgDate(int drugId, String query, String DrugMfgDate)
			throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, DrugMfgDate);
			st.setString(2, "" + drugId);

			int i = st.executeUpdate();
			if(i==0)
				return 1;
			else 
				return 0;
			
			
		
		}
	}
	public static int updatedrugQuantity(int drugId, String query, String DrugQuantity)
			throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, DrugQuantity);
			st.setString(2, "" + drugId);

			int i = st.executeUpdate();
			if(i==0)
				return 1;
			else 
				return 0;
			

			
		}
	}
	public static int updateDoctorName1(int drugId, String query, String DoctorName) throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, DoctorName);
			st.setString(2, "" + drugId);

			int i = st.executeUpdate();
			if(i==0)
				return 1;
			else 
				return 0;


		}
	}
	public static int createDrug(String query, String DrugrName, String DrugQuantity, String MfgDate,
			String ExpiryDate, String PatientName, String DoctorName) throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {

			st.setString(1, DrugrName);
			st.setString(2, DrugQuantity);
			st.setString(3, MfgDate);
			st.setString(4, ExpiryDate);
			st.setString(5, PatientName);
			st.setString(6, DoctorName);

			int i = st.executeUpdate();
			if(i==0)
				return 1;
			else 
				return 0;

		
		}
	}


}
