package com.divergent.cms.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.divergent.cms.dto.Doctor;
import com.divergent.cms.dto.LabTest;
import com.divergent.cms.util.ConnectionUtil;

public class LabTestDao {

	public static void viewTest(String query) throws SQLException, IOException {
		System.out.println("Details of LabTest are given below:");

		List<LabTest> result = new ArrayList<>();

		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {

			LabTest labtest = null;
			ResultSet rs = st.executeQuery(query);

			while (rs.next()) {
				// System.out.println(rs.getString(1) + " " + rs.getString(2) + " " +
				// rs.getString(3));
				labtest = new LabTest();
				labtest.setTestName(rs.getString(1));
				labtest.setTestFees(rs.getInt(2));
				labtest.setPatientName(rs.getString(1));
				labtest.setDoctorName(rs.getString(1));
				result.add(labtest);
			}

		}
	}

	public static void deleteLabTest(int ReportID) throws SQLException, IOException {
		String query = "delete from LabTest where ReportID = ?";
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {

			st.setString(1, "" + ReportID);

			int rowsDeleted = st.executeUpdate();
			if (rowsDeleted > 0) {
				System.out.println("A Report was deleted successfully!");

			}	
		}
	}

	public static int updateDoctorName(int reportId, String query, String updateDoctorName)
			throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, updateDoctorName);
			st.setString(2, "" + reportId);

			int i = st.executeUpdate();

			if(i==0)
				return 1;
			else
				return 0;
			

		}
	}

	public static int updatePatientName(int reportId, String query, String updatePatientName)
			throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, updatePatientName);
			st.setString(2, "" + reportId);

			int i = st.executeUpdate();

			if(i==0)
				return 1;
			else
				return 0;

			

		}
	}

	public static int updateTestFees(int reportId, String query, String updateTestFees)
			throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, updateTestFees);
			st.setString(2, "" + reportId);

			int i = st.executeUpdate();

			if(i==0)
				return 1;
			else
				return 0;

			

		}
	}

	public static int updateTestName(int reportId, String query, String updateTestName)
			throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, updateTestName);
			st.setString(2, "" + reportId);

			int i = st.executeUpdate();

			if(i==0)
				return 1;
			else
				return 0;

			
		}
	}

	public static int createTestName(String query, String TestName, String TestFees, String PatientName,
			String DoctorName) throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {

			st.setString(1, TestName);
			st.setString(2, TestFees);
			st.setString(3, PatientName);
			st.setString(4, DoctorName);

			int i = st.executeUpdate();
			if(i==0)
				return 1;
			else
				return 0;

			
		}
	}

}
