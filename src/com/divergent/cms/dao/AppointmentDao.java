package com.divergent.cms.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.divergent.cms.dto.Appointment;
import com.divergent.cms.util.ConnectionUtil;

public class AppointmentDao {

	public static int createAppointment(String query, String patientName, String patientNumber, String AllottedDoctor,
			String AllottedDate) throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, patientName);
			st.setString(2, patientNumber);
			st.setString(3, AllottedDoctor);
			st.setString(4, AllottedDate);

			int i = st.executeUpdate();
			if(i==0)
				return 1;
			else
				return 0;
			
			
		}
	}
	public static void viewAppointment(String query) throws SQLException, IOException {
		System.out.println("Details of Appointment are given below:");
		List<Appointment> result = new ArrayList<>();
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			
			Appointment appointment = null;
			ResultSet rs = st.executeQuery(query);

			while (rs.next()) {
//				System.out.println(
//						rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " " + rs.getString(4));
//				System.exit(0);
				appointment = new Appointment();
				appointment.setPatientName(rs.getString(1));
				appointment.setPatientNumber(rs.getInt(2));
				appointment.setAllottedDoctor(rs.getString(3));
				appointment.setAllottedDate(rs.getInt(4));
				result.add(appointment);
			}

		}
	}
	
}
