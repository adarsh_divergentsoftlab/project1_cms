package com.divergent.cms.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.divergent.cms.dto.Patient;
import com.divergent.cms.util.ConnectionUtil;

public class PatientCrudDao {

	public static void viewPatient(String query) throws SQLException, IOException {
		System.out.println("Details of Patient are given below:");

		List<Patient> result = new ArrayList<>();
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {

			Patient patient = null;
			ResultSet rs = st.executeQuery(query);

			while (rs.next()) {
//				System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " "
//						+ rs.getString(4) + " " + rs.getString(5));
//				System.exit(0);

				patient = new Patient();
				patient.setFName(rs.getString(1));
				patient.setLName(rs.getString(2));
				patient.setPatientAge(rs.getInt(3));
				patient.setPatientNumber(rs.getInt(4));
				patient.setAddress(rs.getString(5));
				result.add(patient);
			}

		}
	}

	public static void deletePatient(int patientId) throws SQLException, IOException {
		String query = "delete from patient where PatientID = ?";
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {

			st.setString(1, "" + patientId);

			int rowsDeleted = st.executeUpdate();
			if (rowsDeleted > 0) {
				System.out.println("A user was deleted successfully!");

			}
		}
	}

	public static int updateFName(int patientId, String query, String updateFName) throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, updateFName);
			st.setString(2, "" + patientId);

			int i = st.executeUpdate();

			if(i==0)
				return 1;
			else 
				return 0;

			

		}
	}

	public static int updateLName(int patientId, String query, String updateLName) throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, updateLName);
			st.setString(2, "" + patientId);

			int i = st.executeUpdate();

			if(i==0)
				return 1;
			else 
				return 0;

			

		}
	}

	public static int updatePatientAge(int patientId, String query, String updatePatientAge)
			throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, updatePatientAge);
			st.setString(2, "" + patientId);

			int i = st.executeUpdate();
			if(i==0)
				return 1;
			else 
				return 0;


		

		}
	}

	public static int updatePatientNumber(int doctorId, String query, String updatePatientNumber)
			throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, updatePatientNumber);
			st.setString(2, "" + doctorId);

			int i = st.executeUpdate();

			if(i==0)
				return 1;
			else 
				return 0;


			
		}
	}

	public static int updatepatientAddress(int patientId, String query, String updatePatientAddress)
			throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {
			st.setString(1, updatePatientAddress);
			st.setString(2, "" + patientId);

			int i = st.executeUpdate();

			if(i==0)
				return 1;
			else 
				return 0;

		}
	}

	public static int createPatient(String query, String FName, String LName, String PatientAge, String PatientNumber,
			String PatientAddress) throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {

			st.setString(1, FName);
			st.setString(2, LName);
			st.setString(3, PatientAge);
			st.setString(4, PatientNumber);
			st.setString(5, PatientAddress);

			int i = st.executeUpdate();
			if(i==0)
				return 1;
			else 
				return 0;
			
		}
	}
}
