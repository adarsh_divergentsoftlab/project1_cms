package com.divergent.cms.dao;

import java.io.IOException;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

import com.divergent.cms.dto.Invoice;
import com.divergent.cms.util.ConnectionUtil;

public class InvoiceDao {
	
	public static void viewInvoice(String query) throws SQLException, IOException {
		
		List<Invoice> result = new ArrayList<>();
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {

			Invoice invoice = null ;
			ResultSet rs = st.executeQuery(query);

			while (rs.next()) {
//				System.out.println(rs.getString(1) + " " + rs.getString(2) + " " + rs.getString(3) + " "
//						+ rs.getString(4) + " " + rs.getString(5));
//				System.exit(0);
				invoice = new Invoice();
				invoice.setPatientName(rs.getString(1));
				invoice.setTestName(rs.getString(2));
				invoice.setDoctorFees(rs.getInt(3));
				result.add(invoice);
				
//				
			}

		}
	}
	public static int createInvoice(String query, String PatientName, String TestName, String DoctorFees,
			String TestFees) throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); PreparedStatement st = conn.prepareStatement(query)) {

			st.setString(1, PatientName);
			st.setString(2, TestName);
			st.setString(3, DoctorFees);
			st.setString(4, TestFees);

			int i = st.executeUpdate();
			if(i==0)
				return 1;
			else
				return 0;
			
		}
		
	}

}
