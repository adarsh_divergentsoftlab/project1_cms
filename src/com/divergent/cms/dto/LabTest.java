package com.divergent.cms.dto;

public class LabTest {

	private String TestName;
	private int TestFees;
	private String PatientName;
	private String DoctorName;
	public String getTestName() {
		return TestName;
	}
	public void setTestName(String testName) {
		TestName = testName;
	}
	public int getTestFees() {
		return TestFees;
	}
	public void setTestFees(int testFees) {
		TestFees = testFees;
	}
	public String getPatientName() {
		return PatientName;
	}
	public void setPatientName(String patientName) {
		PatientName = patientName;
	}
	public String getDoctorName() {
		return DoctorName;
	}
	public void setDoctorName(String doctorName) {
		DoctorName = doctorName;
	}
	
}
