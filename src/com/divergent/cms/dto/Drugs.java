package com.divergent.cms.dto;

public class Drugs {
	
	private String DrugName;
	private int DrugQuantity;
	private int MgfDate;
	private int ExpiryDate;
	private String patientName;
	private String DoctorName;
	public String getDrugName() {
		return DrugName;
	}
	public void setDrugName(String drugName) {
		DrugName = drugName;
	}
	public int getDrugQuantity() {
		return DrugQuantity;
	}
	public void setDrugQuantity(int drugQuantity) {
		DrugQuantity = drugQuantity;
	}
	public int getMgfDate() {
		return MgfDate;
	}
	public void setMgfDate(int mgfDate) {
		MgfDate = mgfDate;
	}
	public int getExpiryDate() {
		return ExpiryDate;
	}
	public void setExpiryDate(int expiryDate) {
		ExpiryDate = expiryDate;
	}
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getDoctorName() {
		return DoctorName;
	}
	public void setDoctorName(String doctorName) {
		DoctorName = doctorName;
	}

}
