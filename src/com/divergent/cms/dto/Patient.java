package com.divergent.cms.dto;

public class Patient {
	
	private String FName;
	private String LName;
	private int PatientAge;
	private int PatientNumber;
	private String Address;
	public String getFName() {
		return FName;
	}
	public void setFName(String fName) {
		FName = fName;
	}
	public String getLName() {
		return LName;
	}
	public void setLName(String lName) {
		LName = lName;
	}
	public int getPatientAge() {
		return PatientAge;
	}
	public void setPatientAge(int patientAge) {
		PatientAge = patientAge;
	}
	public int getPatientNumber() {
		return PatientNumber;
	}
	public void setPatientNumber(int patientNumber) {
		PatientNumber = patientNumber;
	}
	public String getAddress() {
		return Address;
	}
	public void setAddress(String address) {
		Address = address;
	}

}
