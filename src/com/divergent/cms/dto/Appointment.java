package com.divergent.cms.dto;

public class Appointment {
	private String PatientName;
	private int PatientNumber;
	private int AllottedDate;
	private String AllottedDoctor;
	public String getPatientName() {
		return PatientName;
	}
	public void setPatientName(String patientName) {
		PatientName = patientName;
	}
	public int getPatientNumber() {
		return PatientNumber;
	}
	public void setPatientNumber(int patientNumber) {
		PatientNumber = patientNumber;
	}
	public int getAllottedDate() {
		return AllottedDate;
	}
	public void setAllottedDate(int allottedDate) {
		AllottedDate = allottedDate;
	}
	public String getAllottedDoctor() {
		return AllottedDoctor;
	}
	public void setAllottedDoctor(String allottedDoctor) {
		AllottedDoctor = allottedDoctor;
	}

}
