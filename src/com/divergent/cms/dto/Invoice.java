package com.divergent.cms.dto;

public class Invoice {

	private String patientName;
	private String TestName;
	private int DoctorFees;
	private int testFess;
	public String getPatientName() {
		return patientName;
	}
	public void setPatientName(String patientName) {
		this.patientName = patientName;
	}
	public String getTestName() {
		return TestName;
	}
	public void setTestName(String testName) {
		TestName = testName;
	}
	public int getDoctorFees() {
		return DoctorFees;
	}
	public void setDoctorFees(int doctorFees) {
		DoctorFees = doctorFees;
	}
	public int getTestFess() {
		return testFess;
	}
	public void setTestFess(int testFess) {
		this.testFess = testFess;
	}
}
