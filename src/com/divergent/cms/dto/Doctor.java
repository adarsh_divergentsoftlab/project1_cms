package com.divergent.cms.dto;

public class Doctor {
	
	private String DoctorName;
	private String DoctorSpecialiation;
	private int DoctorFees;
	private String DoctorDegree;
	public String getDoctorName() {
		return DoctorName;
	}
	public void setDoctorName(String doctorName) {
		DoctorName = doctorName;
	}
	public String getDoctorSpecialiation() {
		return DoctorSpecialiation;
	}
	public void setDoctorSpecialiation(String doctorSpecialiation) {
		DoctorSpecialiation = doctorSpecialiation;
	}
	public int getDoctorFees() {
		return DoctorFees;
	}
	public void setDoctorFees(int doctorFees) {
		DoctorFees = doctorFees;
	}
	public String getDoctorDegree() {
		return DoctorDegree;
	}
	public void setDoctorDegree(String doctorDegree) {
		DoctorDegree = doctorDegree;
	}

}
