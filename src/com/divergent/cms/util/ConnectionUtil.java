package com.divergent.cms.util;

import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Properties;

public class ConnectionUtil {
	
	public static Connection getConnection() throws SQLException, IOException {
		var props = new Properties();
		// Path currentDir = Paths.get(".");
		// System.out.println(currentDir.toAbsolutePath());
		// try with resources
		try (InputStream in = Files.newInputStream(Paths.get("./resources/database.properties"))) {
			props.load(in);
		}
		String drivers = props.getProperty("jdbc.drivers");
		if (drivers != null)
			System.setProperty("jdbc.drivers", drivers);
		String url = props.getProperty("jdbc.url");
		String username = props.getProperty("jdbc.username");
		String password = props.getProperty("jdbc.password");
		return DriverManager.getConnection(url, username, password);
	}

	public static int getMaxSupportedStatements() throws SQLException, IOException {
		try (Connection conn = ConnectionUtil.getConnection(); Statement stat = conn.createStatement()) {
			// There are lot of information about the database that we can get from the
			// DatabaseMedaData
			System.out.println(conn.getMetaData().getDatabaseMajorVersion());
			System.out.println(conn.getMetaData().getDatabaseMinorVersion());

			return conn.getMetaData().getMaxConnections();

		}
	}
}
